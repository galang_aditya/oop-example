<?php

class ObatService
{
	protected $obat;

	public function __construct()
	{
		$CI =& get_instance();
		$CI->load->model('obat');
		$this->obat = $CI->obat;

	}

	public function getAll()
	{
		return $this->obat->all();
	}

	public function update($id, array $data)
	{
		$obat = $this->obat->find($id);
		$obat->nama = $data['nama'];
		$obat->jenis = $data['jenis'];
		$obat->save();
	}


	public function store(array $data)
	{
		$obat = $this->obat;
		$obat->nama = $data['nama'];
		$obat->jenis = $data['jenis'];

		$obat->save();
	}

	public function edit($id){
		return $this->obat->find($id);
	}
	
	public function delete(int $id) {

		$obat = $this->obat->find($id);
		return $obat->delete();

	}


}
