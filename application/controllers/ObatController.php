<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

include __DIR__ . '/../Services/ObatService.php';

class ObatController extends CI_Controller {

	protected $obatService;

    public function __construct()
    {
		parent::__construct();
		$this->obatService = new ObatService();
    }

    public function index()
    {
		var_dump($this->obatService->getAll());
    }

	public function store()
	{
		echo $this->obatService->store([
			'nama' => 'Paracetamol',
			'jenis' => 'DPHO'
		]);
	}
	public function show(){
		var_dump($this->obatService->edit(0));

	}
	public function update($id)
	{
		$this->obatService->update($id, $this->input->get());
	}
	public function delete($id)
	{
		$this->obatService->delete($id);
	}
}

/* End of file ObatController.php and path \application\controllers\ObatController.php */
